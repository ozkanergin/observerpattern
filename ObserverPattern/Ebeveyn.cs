﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    public class Ebeveyn : Observer
    {
        private string adi;

        public Ebeveyn(string adi)
        {
            this.adi = adi;
        }


        public void Update(Observable observable)
        {
            Termometre termometre = (Termometre)observable;
            Console.WriteLine(adi + " dedi ki sicaklik : " + termometre.Anlik_Sicaklik);
        }
    }
}
