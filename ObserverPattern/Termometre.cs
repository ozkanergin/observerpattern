﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    public class Termometre : Observable
    {
        private double anliksicaklik;
        private double makssicaklik;
        private double minsicaklik;

        public double Anlik_Sicaklik
        {
            get => anliksicaklik;
            set
            {
                anliksicaklik = value;
                Console.WriteLine(value);
                Sicakligi_Kontrol_Et();
            }
        }

        public Termometre(int makssicaklik, int minsicaklik)
        {
            this.makssicaklik  = makssicaklik;
            this.minsicaklik   = minsicaklik;
            this.anliksicaklik = 24f;
        }


        private void Sicakligi_Kontrol_Et()
        {
            if(anliksicaklik >= makssicaklik || anliksicaklik<= minsicaklik)
            {
                HaberVer();
            }
        }
    }
}
