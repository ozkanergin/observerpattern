﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Termometre termometre = new Termometre(28, 23);

            Ebeveyn anne = new Ebeveyn("anne");
            Ebeveyn baba = new Ebeveyn("baba");

            termometre.Ekle(anne);

            termometre.Anlik_Sicaklik = 29;

            Console.ReadLine();
        }
    }
}
