﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    public abstract class Observable
    {
        private List<Observer> observerList;

        public Observable()
        {
            observerList = new List<Observer>();
        }

        public void Ekle(Observer observer)
        {
            observerList.Add(observer);
        }

        public void Cikar(Observer observer)
        {
            observerList.Remove(observer);
        }

        public void HaberVer()
        {
            foreach (Observer item in observerList)
            {
                item.Update(this);
            }
        }
    }
}
